resource "tls_private_key" "public_private_key_pair_1" {
    algorithm   = "RSA"
}

resource "oci_core_instance" "WorkshopWebserver1" {
    availability_domain = lookup(data.oci_identity_availability_domains.ADs.availability_domains[1], "name")
    compartment_id = var.compartment_ocid
    display_name = "WorkshopWebServer1"
    shape = var.Shapes
    subnet_id = oci_core_subnet.WorkshopWebSubnet.id

    source_details {
        source_type = "image"
        source_id   = lookup(data.oci_core_images.OSImageLocal.images[0], "id")
    }

    metadata = {
        # public key used by the provisioner only + user provided public key:
        ssh_authorized_keys = "${tls_private_key.public_private_key_pair_1.public_key_openssh}${var.public_ssh_key}"
    }

    create_vnic_details {
        subnet_id = oci_core_subnet.WorkshopWebSubnet.id
    }
}

data "oci_core_vnic_attachments" "WorkshopWebserver1_VNIC1_attach" {
    availability_domain = lookup(data.oci_identity_availability_domains.ADs.availability_domains[1], "name")
    compartment_id = var.compartment_ocid
    instance_id = oci_core_instance.WorkshopWebserver1.id
}

data "oci_core_vnic" "WorkshopWebserver1_VNIC1" {
    vnic_id = data.oci_core_vnic_attachments.WorkshopWebserver1_VNIC1_attach.vnic_attachments.0.vnic_id
}

output "WorkshopWebserver1PublicIP" {
    value = [data.oci_core_vnic.WorkshopWebserver1_VNIC1.public_ip_address]
}
