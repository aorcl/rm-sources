resource "oci_load_balancer" "WorkshopLoadBalancer" {
  shape          = "100Mbps"
  compartment_id = var.compartment_ocid
  subnet_ids     = [
    oci_core_subnet.WorkshopWebSubnet.id
  ]
  display_name   = "WorkshopLoadBalancer"
}

resource "oci_load_balancer_backendset" "WorkshopLoadBalancerBackendset" {
  name             = "WorkshopLBBackendset"
  load_balancer_id = oci_load_balancer.WorkshopLoadBalancer.id
  policy           = "ROUND_ROBIN"

  health_checker {
    port     = "80"
    protocol = "HTTP"
    response_body_regex = ".*"
    url_path = "/"
  }
}


resource "oci_load_balancer_listener" "WorkshopLoadBalancerListener" {
  load_balancer_id         = oci_load_balancer.WorkshopLoadBalancer.id
  name                     = "WorkshopLoadBalancerListener"
  default_backend_set_name = oci_load_balancer_backendset.WorkshopLoadBalancerBackendset.name
  port                     = 80
  protocol                 = "HTTP"
}


resource "oci_load_balancer_backend" "WorkshopLoadBalancerBackend" {
  load_balancer_id = oci_load_balancer.WorkshopLoadBalancer.id
  backendset_name  = oci_load_balancer_backendset.WorkshopLoadBalancerBackendset.name
  ip_address       = oci_core_instance.WorkshopWebserver1.private_ip
  port             = 80 
  backup           = false
  drain            = false
  offline          = false
  weight           = 1
}

resource "oci_load_balancer_backend" "WorkshopLoadBalancerBackend2" {
  load_balancer_id = oci_load_balancer.WorkshopLoadBalancer.id
  backendset_name  = oci_load_balancer_backendset.WorkshopLoadBalancerBackendset.name
  ip_address       = oci_core_instance.WorkshopWebserver2.private_ip
  port             = 80
  backup           = false
  drain            = false
  offline          = false
  weight           = 1
}


output "WorkshopLoadBalancer_Public_IP" {
  value = [oci_load_balancer.WorkshopLoadBalancer.ip_addresses]
}

