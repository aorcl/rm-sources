variable "tenancy_ocid" {}
# variable "user_ocid" {}
# variable "fingerprint" {}
# variable "private_key_path" {}
variable "compartment_ocid" {}
variable "region" {}

#variable "NewCompartment" {}

variable "public_ssh_key" {}

variable "VCN-CIDR" {
  default = "10.0.0.0/16"
}

variable "VCNname" {
  default = "WorkshopVCN"
}

variable "Shapes" {
  default = "VM.Standard.E2.1"
}

variable "OsImage" {
  default = "Oracle-Linux-7.9-2020.11.10-1"
}

variable "service_ports" {
  type = list(string)
  default = ["80","443","22"]
}
