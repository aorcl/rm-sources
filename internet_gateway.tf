resource "oci_core_internet_gateway" "WorkshopInternetGateway" {
    compartment_id = var.compartment_ocid
    display_name = "WorkshopInternetGateway"
    vcn_id = oci_core_virtual_network.WorkshopVCN.id
}
