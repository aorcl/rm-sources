resource "oci_core_subnet" "WorkshopWebSubnet" {
  cidr_block = "10.0.1.0/24"
  display_name = "WorkshopWebSubnet"
  dns_label = "WorkshopN1"
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_virtual_network.WorkshopVCN.id
  route_table_id = oci_core_route_table.WorkshopRouteTableViaIGW.id
  dhcp_options_id = oci_core_dhcp_options.WorkshopDhcpOptions1.id
  security_list_ids = [oci_core_security_list.WorkshopSecurityList.id]
}


