# Workshop Terraform and Resource Manager on OCI 

This guide is adapted from a much bigger Terraform course made by Martin Linxfeld from Oracle A-Team:
https://github.com/mlinxfeld/foggykitchen_tf_oci_course 

### Load Balancer and Compute

This code is to be used from OCI Resource manager through gitlab integration.

In this lesson, we will introduce the OCI Public Load Balancer. Load Balancer's Listener entity will be visible on the Internet, which means you will have an additional public IP address. On the other hand Load Balancer's Backendset with Backends will be associated with both Webserver VMs. The outcome of this training is very simple. You can access web servers via the Load Balancer. Reload webpage a couple of times and you should expect index.html page to be different depends on what web server has been chosen by the Load Balancer. 

![](LESSON3_load_balancer.jpg)
