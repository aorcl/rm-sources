resource "oci_core_dhcp_options" "WorkshopDhcpOptions1" {
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_virtual_network.WorkshopVCN.id
  display_name = "WorkshopDHCPOptions1"

  // required
  options {
    type = "DomainNameServer"
    server_type = "VcnLocalPlusInternet"
  }

  // optional
  options {
    type = "SearchDomain"
    search_domain_names = [ "oracle.com" ]
  }
}
